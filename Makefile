tidy:
					go mod tidy

mod-reset:
					rm -rf ./go.sum && go clean --modcache && go mod tidy\

run:
					go run cmd/http/api.go
