package helper

import (
	"bytes"
	"encoding/json"
	"net/http"
)

type RestTransport struct {
	Client  *http.Client
	Url     string
	Method  string
	Header  http.Header
	Payload interface{}
}

func NewRestTransport(httpClient *http.Client) *RestTransport {
	return &RestTransport{
		Client: httpClient,
	}
}

func Request(req RestTransport) (res *http.Response, err error) {
	var body *bytes.Buffer
	var httpReq *http.Request

	if req.Payload != nil {
		b, err := json.Marshal(req.Payload)
		if err != nil {
			return res, err
		}
		body = bytes.NewBuffer(b)
	}

	if req.Payload == nil {
		httpReq, err = http.NewRequest(req.Method, req.Url, nil)
	} else {
		httpReq, err = http.NewRequest(req.Method, req.Url, body)
	}
	if err != nil {
		return res, err
	}
	// defer httpReq.Body.Close()

	if req.Header != nil {
		httpReq.Header = req.Header
	}

	res, err = req.Client.Do(httpReq)
	if err != nil {
		return res, err
	}

	return res, nil
}
