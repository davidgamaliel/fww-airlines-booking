package provider

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	"github.com/fww/core/config"
	"github.com/fww/core/internal/helper"
	"go.uber.org/zap"
)

type Partner struct {
	Base          *config.BaseDep
	Url           string
	TlsSkipVerify bool
}

type PartnerProvider interface {
	GetDisdukcapilCitizen(id string) (*Citizen, error)
	CheckDisdukcapilCitizenBlacklist(id string) (bool, error)
	CheckCovidStatus(id string) (SehatRes, error)
}

func NewPartnerProvider(p Partner) PartnerProvider {
	return &p
}

func (p *Partner) GetDisdukcapilCitizen(id string) (*Citizen, error) {
	httpRes, err := helper.Request(helper.RestTransport{
		Client:  p.getHttpClient(),
		Url:     p.Url + "/disdukcapil/api/v1/citizen/" + id,
		Method:  "GET",
		Header:  getHttpHeader(""),
		Payload: nil,
	})

	if err != nil {
		return nil, err
	}
	defer httpRes.Body.Close()

	if httpRes.StatusCode == http.StatusNotFound {
		return nil, nil
	}

	res, err := parseCitizenResponseBody(httpRes.Body)
	if err != nil {
		p.Base.Logger.Error(
			"failed to parse response body",
			zap.Error(err),
		)
		return nil, err
	}

	return &res, nil
}

func (p *Partner) CheckDisdukcapilCitizenBlacklist(id string) (bool, error) {
	httpRes, err := helper.Request(helper.RestTransport{
		Client:  p.getHttpClient(),
		Url:     fmt.Sprintf("%s/disdukcapil/api/v1/citizen/%s/blacklists", p.Url, id),
		Method:  "GET",
		Header:  getHttpHeader(""),
		Payload: nil,
	})

	if err != nil {
		return true, err
	}
	defer httpRes.Body.Close()

	if httpRes.StatusCode == http.StatusNotFound {
		return true, nil
	}

	res, err := parseBlackistResponseBody(httpRes.Body)
	if err != nil {
		p.Base.Logger.Error(
			"failed to parse response body",
			zap.Error(err),
		)
		return true, err
	}

	return res, nil
}

func (p *Partner) CheckCovidStatus(id string) (SehatRes, error) {
	httpRes, err := helper.Request(helper.RestTransport{
		Client:  p.getHttpClient(),
		Url:     fmt.Sprintf("%s/sehat/api/v1/users/%s/status", p.Url, id),
		Method:  "GET",
		Header:  getHttpHeader(""),
		Payload: nil,
	})

	if err != nil {
		return SehatRes{}, err
	}
	defer httpRes.Body.Close()

	if httpRes.StatusCode/100 != 2 {
		return SehatRes{}, fmt.Errorf("request failed %d", httpRes.StatusCode)
	}

	res, err := parseSehatResponseBody(httpRes.Body)
	if err != nil {
		p.Base.Logger.Error(
			"failed to parse response body",
			zap.Error(err),
		)
		return SehatRes{}, err
	}

	return *res, nil
}

func getHttpHeader(token string) http.Header {
	header := http.Header{}
	header.Set("Content-Type", "application/json")
	if token != "" {
		header.Set("Authorization", fmt.Sprintf("Basic %s", token))
	}
	return header
}

func (p *Partner) getHttpClient() *http.Client {
	transport := &http.Transport{}

	client := &http.Client{
		Timeout: 1 * time.Minute,
	}

	transport.TLSClientConfig = &tls.Config{
		InsecureSkipVerify: p.TlsSkipVerify,
	}
	client.Transport = transport

	return client
}

func parseCitizenResponseBody(body io.ReadCloser) (Citizen, error) {
	var res SuccessCitizenResponse
	b, err := io.ReadAll(body)
	if err != nil {
		return Citizen{}, err
	}

	err = json.Unmarshal(b, &res)
	if err != nil {
		return Citizen{}, err
	}

	return res.Data, nil
}

func parseBlackistResponseBody(body io.ReadCloser) (bool, error) {
	var res SuccessBlacklistResponse
	b, err := io.ReadAll(body)
	if err != nil {
		return true, err
	}

	err = json.Unmarshal(b, &res)
	if err != nil {
		return true, err
	}

	return res.Data, nil
}

func parseSehatResponseBody(body io.ReadCloser) (*SehatRes, error) {
	var res SuccessSehatResponse
	b, err := io.ReadAll(body)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(b, &res)
	if err != nil {
		return nil, err
	}

	return &res.Data, nil
}
