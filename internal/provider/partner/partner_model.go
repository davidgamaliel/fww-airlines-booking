package provider

import "github.com/fww/core/internal/model"

type Citizen struct {
	ID     string `json:"id"`
	Name   string `json:"name"`
	Status string `json:"status"`
}

type SuccessCitizenResponse struct {
	Data Citizen    `json:"data"`
	Meta model.Meta `json:"meta"`
}

type SuccessBlacklistResponse struct {
	Data bool       `json:"data"`
	Meta model.Meta `json:"meta"`
}

type SehatRes struct {
	ID            string `json:"id"`
	Status        string `json:"status"`
	VaccineFirst  string `json:"vaccine_first"`
	VaccineSecond string `json:"vaccine_second"`
	VaccineThird  string `json:"vaccine_third"`
}

type SuccessSehatResponse struct {
	Data SehatRes   `json:"data"`
	Meta model.Meta `json:"meta"`
}
