package provider

import (
	"os"
	"time"

	camundaclientgo "github.com/citilinkru/camunda-client-go/v3"
	"github.com/fww/core/config"
	"go.uber.org/zap"
)

type Camunda struct {
	Base   *config.BaseDep
	Client *camundaclientgo.Client
}

type CamundaProvider interface {
	StartProcess(req CamundaProcessRequest) error
	GetClient() *camundaclientgo.Client
}

func NewCamundaClient(base *config.BaseDep) CamundaProvider {
	return &Camunda{
		Base: base,
		Client: camundaclientgo.NewClient(camundaclientgo.ClientOptions{
			EndpointUrl: os.Getenv("CAMUNDA_ENDPOINT"),
			ApiUser:     os.Getenv("CAMUNDA_USER"),
			ApiPassword: os.Getenv("CAMUNDA_PASSWORD"),
			Timeout:     time.Second * 100,
		}),
	}
}

func (p *Camunda) StartProcess(req CamundaProcessRequest) error {
	res, err := p.Client.ProcessDefinition.StartInstance(
		camundaclientgo.QueryProcessDefinitionBy{Key: &req.Key},
		camundaclientgo.ReqStartInstance{Variables: &req.Variables},
	)

	if err != nil {
		p.Base.Logger.Error("failed start camunda process", zap.Error(err))
		return err
	}

	p.Base.Logger.Info(
		"success start camunda process",
		zap.Any("result", res.Variables),
	)

	return nil
}

func (p *Camunda) GetClient() *camundaclientgo.Client {
	return p.Client
}
