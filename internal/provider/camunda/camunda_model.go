package provider

import camundaclientgo "github.com/citilinkru/camunda-client-go/v3"

type CamundaProcessRequest struct {
	Key       string
	Variables map[string]camundaclientgo.Variable
}
