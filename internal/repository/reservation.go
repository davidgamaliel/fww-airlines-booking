package repository

import (
	"github.com/fww/core/config"
	"github.com/fww/core/internal/model"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

type ReservationRepository struct {
	BaseDep *config.BaseDep
	Pool    *gorm.DB
}

type ReservationPersister interface {
	CreateReservation(req model.Reservation) (model.Reservation, error)
}

func NewReservationPersister(r ReservationRepository) ReservationPersister {
	return &r
}

func (r *ReservationRepository) CreateReservation(req model.Reservation) (model.Reservation, error) {
	res := r.Pool.Create(&req)
	if res.Error != nil {
		r.BaseDep.Logger.Error("failed create reservation", zap.Error(res.Error))
		return model.Reservation{}, res.Error
	}

	return req, nil
}
