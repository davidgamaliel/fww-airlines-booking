package usecase

import (
	camundaclientgo "github.com/citilinkru/camunda-client-go/v3"
	"github.com/citilinkru/camunda-client-go/v3/processor"
	"github.com/fww/core/config"
	"github.com/fww/core/internal/model"
	cProvider "github.com/fww/core/internal/provider/camunda"
	pProvider "github.com/fww/core/internal/provider/partner"
	"github.com/fww/core/internal/repository"
)

type ReservationUsecase struct {
	BaseDep    *config.BaseDep
	Repository repository.ReservationPersister
	Camunda    cProvider.CamundaProvider
	Partner    pProvider.PartnerProvider
}

type ReservationExecutor interface {
	CreateReservation(req model.Reservation) error
	DisdukcapilCheck() ([]*camundaclientgo.QueryFetchAndLockTopic, processor.Handler)
	ImmigrationCheck() ([]*camundaclientgo.QueryFetchAndLockTopic, processor.Handler)
	StatusSehatCheck() ([]*camundaclientgo.QueryFetchAndLockTopic, processor.Handler)
}

func NewReservationExecutor(r ReservationUsecase) ReservationExecutor {
	return &r
}

func (u *ReservationUsecase) CreateReservation(req model.Reservation) error {
	res, err := u.Repository.CreateReservation(req)
	if err != nil {
		return err
	}

	params := map[string]camundaclientgo.Variable{
		"isMember": {Value: "Yes", Type: "string"},
		"userID":   {Value: res.UserID, Type: "integer"},
		"ktp":      {Value: res.KTP, Type: "string"},
	}
	err = u.Camunda.StartProcess(cProvider.CamundaProcessRequest{
		Key:       "FWW_Reservation_Ticket",
		Variables: params,
	})

	return nil
}
