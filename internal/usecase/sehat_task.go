package usecase

import (
	"fmt"
	"time"

	camundaclientgo "github.com/citilinkru/camunda-client-go/v3"
	"github.com/citilinkru/camunda-client-go/v3/processor"
	"go.uber.org/zap"
)

func (u *ReservationUsecase) StatusSehatCheck() ([]*camundaclientgo.QueryFetchAndLockTopic, processor.Handler) {
	topic := []*camundaclientgo.QueryFetchAndLockTopic{
		{TopicName: "CheckPeduliLindungi"},
	}

	return topic, func(ctx *processor.Context) error {
		fmt.Printf("Running task %s. WorkerId: %s. TopicName: %s\n", ctx.Task.Id, ctx.Task.WorkerId, ctx.Task.TopicName)

		vars := ctx.Task.Variables
		// u.BaseDep.Logger.Info(ctx.Task.TopicName, zap.Any("variables", vars))
		id := vars["ktp"].Value
		statusCovid := "safe"
		time.Sleep(time.Second * 1)
		status, err := u.Partner.CheckCovidStatus(id.(string))
		if err != nil {
			u.BaseDep.Logger.Error("failed get citizen", zap.Error(err))
			statusCovid = "not safe"
		}
		statusCovid = status.Status

		err = ctx.Complete(processor.QueryComplete{
			Variables: &map[string]camundaclientgo.Variable{
				"statusCovid": {Value: statusCovid, Type: "string"},
			},
		})
		if err != nil {
			fmt.Printf("Error set complete task %s: %s\n", ctx.Task.Id, err)
		}

		fmt.Printf("Task %s completed\n", ctx.Task.Id)
		return nil
	}
}
