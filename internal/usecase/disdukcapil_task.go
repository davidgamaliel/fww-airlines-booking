package usecase

import (
	"fmt"
	"time"

	camundaclientgo "github.com/citilinkru/camunda-client-go/v3"
	"github.com/citilinkru/camunda-client-go/v3/processor"
	"go.uber.org/zap"
)

func (u *ReservationUsecase) DisdukcapilCheck() ([]*camundaclientgo.QueryFetchAndLockTopic, processor.Handler) {
	topic := []*camundaclientgo.QueryFetchAndLockTopic{
		{TopicName: "CheckDisdukcapil"},
	}

	return topic, func(ctx *processor.Context) error {
		fmt.Printf("Running task %s. WorkerId: %s. TopicName: %s\n", ctx.Task.Id, ctx.Task.WorkerId, ctx.Task.TopicName)

		vars := ctx.Task.Variables
		// u.BaseDep.Logger.Info(ctx.Task.TopicName, zap.Any("variables", vars))
		id := vars["ktp"].Value
		isFound := true
		time.Sleep(time.Second * 1)
		citizen, err := u.Partner.GetDisdukcapilCitizen(id.(string))
		if err != nil {
			u.BaseDep.Logger.Error("failed get citizen", zap.Error(err))
			isFound = false
		}
		if citizen == nil {
			isFound = false
		}

		err = ctx.Complete(processor.QueryComplete{
			Variables: &map[string]camundaclientgo.Variable{
				"isFound": {Value: isFound, Type: "boolean"},
			},
		})
		if err != nil {
			fmt.Printf("Error set complete task %s: %s\n", ctx.Task.Id, err)
		}

		fmt.Printf("Task %s completed\n", ctx.Task.Id)
		return nil
	}
}

func (u *ReservationUsecase) ImmigrationCheck() ([]*camundaclientgo.QueryFetchAndLockTopic, processor.Handler) {
	topic := []*camundaclientgo.QueryFetchAndLockTopic{
		{TopicName: "CheckImigrasi"},
	}

	return topic, func(ctx *processor.Context) error {
		fmt.Printf("Running task %s. WorkerId: %s. TopicName: %s\n", ctx.Task.Id, ctx.Task.WorkerId, ctx.Task.TopicName)

		time.Sleep(time.Second * 1)
		vars := ctx.Task.Variables
		// u.BaseDep.Logger.Info(ctx.Task.TopicName, zap.Any("variables", vars))
		id := vars["ktp"].Value
		time.Sleep(time.Second * 1)
		isBlacklist, err := u.Partner.CheckDisdukcapilCitizenBlacklist(id.(string))
		if err != nil {
			u.BaseDep.Logger.Error("failed get citizen", zap.Error(err))
		}

		err = ctx.Complete(processor.QueryComplete{
			Variables: &map[string]camundaclientgo.Variable{
				"isBlacklist": {Value: isBlacklist, Type: "boolean"},
			},
		})
		if err != nil {
			fmt.Printf("Error set complete task %s: %s\n", ctx.Task.Id, err)
		}

		fmt.Printf("Task %s completed\n", ctx.Task.Id)
		return nil
	}
}
