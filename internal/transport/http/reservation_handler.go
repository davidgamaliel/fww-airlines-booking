package httptransport

import (
	"net/http"
	"time"

	"github.com/fww/core/config"
	"github.com/fww/core/internal/model"
	"github.com/fww/core/internal/usecase"
	"github.com/gofiber/fiber/v2"
)

type Reservation struct {
	Base    *config.BaseDep
	Usecase usecase.ReservationExecutor
}

type ReservationHandler interface {
	CreateTransaction(c *fiber.Ctx) error
}

func NewReservationHandler(h Reservation) ReservationHandler {
	return &h
}

func (h *Reservation) CreateTransaction(c *fiber.Ctx) error {
	err := h.Usecase.CreateReservation(model.Reservation{
		FlightID:  1,
		UserID:    1,
		KTP:       "112010011",
		Status:    0,
		CreatedAT: time.Now(),
		UpdatedAT: time.Now(),
	})

	if err != nil {
		return c.Status(http.StatusBadRequest).JSON(fiber.Map{
			"error": err.Error(),
		})
	}
	return c.Status(http.StatusOK).JSON(fiber.Map{
		"message": "success",
	})
}
