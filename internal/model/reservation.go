package model

import "time"

type Reservation struct {
	ID        int64     `db:"id"`
	FlightID  int64     `db:"flight_id"`
	UserID    int64     `db:"user_id"`
	KTP       string    `db:"ktp"`
	Status    int       `db:"status"`
	CreatedAT time.Time `db:"created_at"`
	UpdatedAT time.Time `db:"updated_at"`
}

func (Reservation) TableName() string {
	return "reservations"
}

type PaymentTransaction struct {
	ID            int64     `db:"id"`
	ReservationID int64     `db:"reservation_id"`
	Code          string    `db:"code"`
	Status        string    `db:"status"`
	CreatedAT     time.Time `db:"created_at"`
	UpdatedAT     time.Time `db:"updated_at"`
}
