package main

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/fww/core/config"
	provider "github.com/fww/core/internal/provider/camunda"
	pProvider "github.com/fww/core/internal/provider/partner"
	"github.com/fww/core/internal/repository"
	httptransport "github.com/fww/core/internal/transport/http"
	"github.com/fww/core/internal/usecase"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/pprof"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/joho/godotenv"
)

func main() {
	loadEnv()
	baseDep := config.NewBaseDep("fww-core")
	config.InitializeDBPoolConn()

	bpm := provider.NewCamundaClient(baseDep)

	partnerProvider := pProvider.NewPartnerProvider(pProvider.Partner{
		Base:          baseDep,
		Url:           os.Getenv("PARTNER_BASE_URL"),
		TlsSkipVerify: true,
	})

	reservationRepo := repository.NewReservationPersister(repository.ReservationRepository{
		BaseDep: baseDep,
		Pool:    config.Pool,
	})

	reservationUsecase := usecase.NewReservationExecutor(usecase.ReservationUsecase{
		BaseDep:    baseDep,
		Repository: reservationRepo,
		Camunda:    bpm,
		Partner:    partnerProvider,
	})

	reservationHandler := httptransport.NewReservationHandler(httptransport.Reservation{
		Base:    baseDep,
		Usecase: reservationUsecase,
	})

	app := fiber.New()
	app.Use(recover.New())
	app.Use(cors.New())
	app.Use(pprof.New())
	app.Use(logger.New(logger.Config{
		Format:       "[${time}] ${status} - ${latency} ${method} ${path}\n",
		TimeInterval: time.Millisecond,
		TimeFormat:   "02-01-2006 15:04:05",
		TimeZone:     "Indonesia/Jakarta",
	}))

	app.Post("/api/v1/reservations", reservationHandler.CreateTransaction)

	bpmProcessor := config.NewBpmProcessor(bpm.GetClient(), baseDep)
	bpmProcessor.RegisterHandler(reservationUsecase.DisdukcapilCheck())
	bpmProcessor.RegisterHandler(reservationUsecase.ImmigrationCheck())
	bpmProcessor.RegisterHandler(reservationUsecase.StatusSehatCheck())

	if err := app.Listen(fmt.Sprintf(":%s", os.Getenv("APP_PORT"))); err != nil {
		log.Fatal(err)
	}

}

func loadEnv() {
	_, err := os.Stat(".env")
	if err == nil {
		err = godotenv.Load()
		if err != nil {
			panic("failed load env file")
		}
	}
}
