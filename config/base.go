package config

import (
	"fmt"
	"net/url"
	"os"
	"strconv"
	"time"
)

type BaseDep struct {
	Logger Logger
	Metric *MetricAgent
}

func NewBaseDep(namespace string) *BaseDep {
	return &BaseDep{
		Logger: NewLogger(),
		Metric: NewMetricAgent(namespace),
	}
}

func Dsn() string {
	return fmt.Sprintf(
		"host=%s user=%s password=%s dbname=%s port=%s sslmode=disable",
		os.Getenv("DATABASE_HOST"),
		os.Getenv("DATABASE_USER"),
		os.Getenv("DATABASE_PASSWORD"),
		os.Getenv("DATABASE_SCHEMA"),
		os.Getenv("DATABASE_PORT"),
	)
}

func PingInterval() time.Duration {
	interval, err := strconv.Atoi(os.Getenv("DATABASE_PING_INTERVAL"))
	if err != nil {
		interval = DEFAULT_PING_INTERVAL
	}
	return time.Duration(interval) * time.Millisecond
}

func DatabasePingInterval() time.Duration {
	return time.Duration(convertStringToInt(os.Getenv("DATABASE_PING_INTERVAL"))) * time.Millisecond
}

func LogLevel() string {
	return os.Getenv("LOG_LEVEL")
}

func PostgreMaxIdleConns() int {
	return convertStringToInt(os.Getenv("DATABASE_MAX_IDLE_CONN"))
}

func PostgreConnMaxLifetime() time.Duration {
	return time.Duration(convertStringToInt(os.Getenv("DATABASE_CONN_MAX_LIFETIME"))) * time.Millisecond
}

func Port() string {
	return os.Getenv("APP_PORT")
}

func convertStringToInt(val string) int {
	intVar, _ := strconv.Atoi(val)
	return intVar
}

func HermesMailURL() string {
	return os.Getenv("URL_HERMES")

}

func TiaMoanaNotifyURL() string {
	return os.Getenv("URL_TIA_MOANA_NOTIFICATION")
}

func TokenMoanaNotification() string {
	return os.Getenv("TOKEN_BEARER_NOTIFICATION")
}

func ServiceEmployeeURL() string {
	return os.Getenv("URL_SERVICE_EMPLOYEE")
}

func TiaWorkflowURL() string {
	return os.Getenv("URL_TIA_WORKFLOW")
}

func EbsWorkingHourURL() string {
	return os.Getenv("URL_EBS_WORKING_HOUR")
}

func EbsLeaveInfoURL() string {
	return os.Getenv("URL_EBS_LEAVE_INFO")
}

func TiaBearerToken() string {
	return os.Getenv("TIA_BEARER_TOKEN")
}

func EbsBasicToken() string {
	return os.Getenv("EBS_BASIC_TOKEN")
}

func GetBaseUrl() string {
	return os.Getenv("BASE_URL")
}

func GetProxyUrl() *url.URL {
	proxyUrl, err := url.Parse(os.Getenv("TIA_PROXY_HOST") + ":" + os.Getenv("TIA_PROXY_PORT"))
	if err != nil {
		return nil
	}

	return proxyUrl
}

func GetCacherAdress() string {
	return fmt.Sprintf("%s:%s", os.Getenv("CACHER_HOST"), os.Getenv("CACHER_PORT"))
}

func GetCacherPassword() string {
	return os.Getenv("CACHER_PASSWORD")
}

func GetCacherService() string {
	return os.Getenv("CACHER_SERVICE")
}

func GetCacherDefaultExp() time.Duration {
	duration, _ := time.ParseDuration(os.Getenv("CACHER_DEFAULT_EXP"))
	return duration
}
