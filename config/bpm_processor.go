package config

import (
	"os"
	"time"

	camundaclientgo "github.com/citilinkru/camunda-client-go/v3"
	"github.com/citilinkru/camunda-client-go/v3/processor"
	"go.uber.org/zap"
)

type BpmProcessor struct {
	Base    *BaseDep
	Client  *camundaclientgo.Client
	Handler *processor.Processor
}

func NewBpmProcessor(c *camundaclientgo.Client, base *BaseDep) BpmProcessor {
	log := func(err error) {
		base.Logger.Error("failed", zap.Error(err))
	}

	return BpmProcessor{
		Base:   base,
		Client: c,
		Handler: processor.NewProcessor(c, &processor.Options{
			WorkerId:                  os.Getenv("CAMUNDA_PROCESSOR_WORKER_ID"),
			LockDuration:              time.Second * 5,
			MaxTasks:                  10,
			MaxParallelTaskPerHandler: 100,
			LongPollingTimeout:        5 * time.Second,
		}, log),
	}
}

func (p *BpmProcessor) RegisterHandler(
	topics []*camundaclientgo.QueryFetchAndLockTopic,
	handler processor.Handler,
) {
	p.Handler.AddHandler(topics, handler)
}

func (p *BpmProcessor) Start() {
	for {
		time.Sleep(time.Second * 100)
	}
}
