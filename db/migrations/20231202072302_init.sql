-- migrate:up
create table if not exists reservations (
    id bigserial primary key,
    flight_id bigint not null,
    user_id bigint not null,
    ktp VARCHAR (25),
    status int not null DEFAULT 0, -- created, processed, rejected, cancelled, paid, finished
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);

create table if not exists payment_transactions (
    id bigserial primary key,
    reservation_id bigint not null,
    code VARCHAR(50) not null,
    status int not null DEFAULT 0, -- created, expired, finished
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


-- migrate:down
drop table if exists payment_transactions;
drop table if exists reservations;